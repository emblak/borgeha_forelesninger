# Hva gjorde vi sist
# Oppsummeringssider - powerpoint - nå med to ekstra sider!

# Dagens tema: løkker og sløyfer
# while
# for


# Oppgave 1:
# Be brukeren om å skrive inn en streng som er enten 'krone' eller 'mynt'
# Hvis svaret er 'krone' skriver du ut 'krone'
# Hvis svaret er 'mynt' skriver du ut 'mynt'
# ellers skriver du ut 'feil svar'
'''
svar = input("krone eller mynt(m): ")

if svar == 'krone':
    print('krone')  
elif svar == 'mynt':
    print('mynt')
else:
    print('feil svar')
'''

# Oppgave 2: brukeren kan skrive 'k' eller 'm' også!
# hvordan skal du snike inn en or i de to testene?
# Dette er en utrolig vanlig feil: tall == 3 or 4  
'''
svar = input("krone(k) eller mynt(m): ")

if svar == 'k' or svar == 'krone':
    print('krone')  
elif svar == 'm' or svar == 'mynt':
    print('mynt')
else:
    print('feil svar')
'''

# Oppgave 3: gjør dette i en uendelig løkke
# tips: hvis du skal 'lempe' en de linjer ett hakk mot høyre,
# da kan du markere dem og trykke på <tab> (shift-tab mot venstre)
'''
while True:
    svar = input("krone(k) eller mynt(m): ")

    if svar == 'k' or svar == 'krone':
        print('krone')  
    elif svar == 'm' or svar == 'mynt':
        print('mynt')
    else:
        print('feil svar')
'''

# Oppgave 4
# Lag to variable, krone og mynt, FØR while-løkken, begge 0.
# Hvorfor må disse variablene deklareres før løkken og ikke inni?
# Øk respektive variabel med 1 basert på om det blir krone eller mynt.
# Hver gang gjennom løkken skal du til slutt skrive ut
# hvor mange krone og mynt brukeren har skrevet inn til nå.

'''
krone, mynt = 0,0
while True:
    svar = input("krone(k) eller mynt(m): ")

    if svar == 'k' or svar == 'krone':
        print('krone')
        krone += 1
    elif svar == 'm' or svar == 'mynt':
        print('mynt')
        mynt += 1
    else:
        print('feil svar')
    print(f'Krone: {krone}, mynt: {mynt}')
'''

# Oppgave 5: Tell antall ganger du har kastet
# Hvor må du deklarere denne variabelen for at den skal
# 'holde seg' hver gang gjennom løkken?
# Bonuspoeng hvis du skriver ut prosentandel krone hver runde
'''
krone = 0
mynt = 0
kast = 0
while True:
    svar = input("krone(k) eller mynt(m): ")

    if svar == 'k' or svar == 'krone':
        print('krone')
        krone += 1
    elif svar == 'm' or svar == 'mynt':
        print('mynt')
        mynt += 1
    else:
        print('feil svar')
    kast += 1
    print(f'Kast: {kast}, krone: {krone}, mynt: {mynt}')
    print(f'Prosentandel krone: {(krone/kast)*100:>4.2f}') # Og litt fint formatert
    # En student postet denne løsningen på forelesning
    print(f'Prosentandel krone: {(krone/(krone+mynt)):.2%}')
    # En mye vakrere løsning! Det kule med Python er at det er så mange veier til målet.
'''

# Oppgave 6: Avslutt etter ti kast!
# while tester om noe er sant. Er det sant, da kjører den løkken
# Hvis ikke, da avslutter den løkken og går videre
# Hvordan kan du endre while-testen så den returnerer sant hvis og bare hvis
# variabelen kast er ti eller mindre?
'''
krone = 0
mynt = 0
kast = 0
while kast < 10: # Vi starter med 0, så da blir ti kast 0 - 9
    svar = input("krone(k) eller mynt(m): ")

    if svar == 'k' or svar == 'krone':
        print('krone')
        krone += 1
    elif svar == 'm' or svar == 'mynt':
        print('mynt')
        mynt += 1
    else:
        print('feil svar')
    kast += 1
    print(f'Kast: {kast}, krone: {krone}, mynt: {mynt}')
    print(f'Prosentandel krone: {(krone/kast)*100:>4.2f}') # Og litt fint formatert
'''


# Oppgave 7: Rensk opp
# Fjern unødvendige utskrifter
# Endre slik at du skriver ut antall kron, mynt (og prosentandel mynt)
# etter at alle er skrevet inn
'''
krone = 0
mynt = 0
kast = 0
while kast < 10: # Vi starter med 0, så da blir ti kast 0 - 9
    svar = input("krone(k) eller mynt(m): ")

    if svar == 'k' or svar == 'krone':
        krone += 1
    elif svar == 'm' or svar == 'mynt':
        mynt += 1
    else:
        print('feil svar')
    kast += 1
print(f'Kast: {kast}, krone: {krone}, mynt: {mynt}')
print(f'Prosentandel krone: {(krone/kast)*100:>4.2f}') # Og litt fint formatert
'''



# Oppgave 8: hvorfor 10 kast?
# Spør brukeren hvor mange ganger det skal kastes
# hvordan kan en bruke dette rett i while-testen?
'''
krone = 0
mynt = 0
kast = int(input("Hvor mange kast?"))
while kast > 0: # Hvis vi teller nedover med 1 hver gang...
    svar = input("krone(k) eller mynt(m): ")

    if svar == 'k' or svar == 'krone':
        krone += 1
    elif svar == 'm' or svar == 'mynt':
        mynt += 1
    else:
        print('feil svar')
    kast -= 1
# Under: Ser du at jeg plutselig ikke kan bruke variabelen kast lenger?
# Hvorfor ikke? Hva gjør jeg med den over? Men det var en omvei!
print(f'Kast: {krone+mynt}, krone: {krone}, mynt: {mynt}')
print(f'Prosentandel krone: {(krone/(krone+mynt))*100:>4.2f}') 
'''


# Oppgave 9: hvorfor bestemme kast først?
# Det er mye bedre å spørre brukeren om hen skal kaste en
# gang til.

krone = 0
mynt = 0
igjen = True
svar = input("krone(k) eller mynt(m) (q avslutter): ")
while svar != 'q':
    
    if svar == 'k' or svar == 'krone':
        krone += 1
    elif svar == 'm' or svar == 'mynt':
        mynt += 1
    else:
        print('feil svar')
    svar = input("krone(k) eller mynt(m) (q avslutter): ")
# Under: her feiler utregningen hvis en aldri kaster. Legger derfor til
# en liten sjekk for å se om det faktisk ble kastet noe.
if krone + mynt > 0:
    print(f'Kast: {krone+mynt}, krone: {krone}, mynt: {mynt}')
    print(f'Prosentandel krone: {(krone/(krone+mynt))*100:>4.2f}') 




