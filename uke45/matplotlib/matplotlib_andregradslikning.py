import matplotlib
import matplotlib.pyplot as plt
import numpy as np # Data for plotting

t = np.arange(-2.0, 2.0, 0.01) # Fra -2 til 2 i steg på 0.01
# t blir vektoren [0.00, 0.01, 0.02, 0.03,...1.99]

s = t**2 
# her regnes s = t**2 for hver verdi av t
# og dette legges inn i vektoren s
fig, ax = plt.subplots()
# genererer grafen og returnerer en referanse til figuren (til fig)
# og en liste over aksene (ax)

ax.plot(t, s) # plotter selve grafen
ax.set(xlabel='x', ylabel='y', title='Parabel')
# setter navn på aksene

ax.grid() # tegner en grid på grafen

fig.savefig("parabelen_min.png")
# Lagrer grafen som en png-fil

plt.show() # Viser vinduet med grafen